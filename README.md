# paprika

Example web application to demonstrate Dockerfile, Gitlab Pipeline and AWS infra setup 

- Listens on port 8000
- Want `ENV` set as environment variable 
- Want `DB_PASSWORD` set as enviroment variable via secrets injection
