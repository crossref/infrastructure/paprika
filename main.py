import os
from fastapi import FastAPI

app = FastAPI()

environment = os.environ.get("ENV", "local")
db_password = os.environ.get("DB_PASSWORD", "supersecret_EF99s04f2O8GMYxI8iFs")

@app.get("/")
async def root():
    message = f"Hello World. Environment is {environment} and db_password is {db_password}"
    return {"message": message}
